## FR :fr:

Ce projet personnel consiste à écrire des défis informatiques sur diverses thématiques.
Il a pour objectif d'enseigner / faire découvrir des notions informatiques de manière ludique et amusante.

Les défis sont regroupés par saison.

Pour chaque saison, dans le fichier **instruction.pdf**, vous retrouverez les instructions relatives aux différents défis.
Quant à leur correction, il est lui consultable dans le fichier **solution.pdf**

Merci de vous reporter à ces fichiers pour pouvoir réaliser les défis et/ou consulter leurs solutions.

haveFun() :fire: !

---

Ci-dessous la liste des saisons actuellement réalisées :

* **SAISON 1** *(2021)* :
	- DÉFI 1 : *Système d'exploitation (GNU/Linux)* :computer:
	- DÉFI 2 : *Programmation (Bash)* :memo:
	- DÉFI 3 : *Programmation* :memo:
	- DÉFI 4 : *Web* :art:
	- DÉFI 5 : *Système d'exploitation (GNU/Linux)* :computer:
	- DÉFI 6 : *Programmation* :memo:

* **SAISON 2** *(2022)* :
	- DÉFI 1 : *Programmation* :memo:
	- DÉFI 2 : *Système d'exploitation (GNU/Linux)* :computer:
	- DÉFI 3 : *Programmation & Réseau* :memo: :globe_with_meridians:
	- DÉFI 4 : *Base de données* :bar_chart:
	- DÉFI 5 : *Web* :art:

* **SAISON 3** *(2023)* :
	- DÉFI 1 : *Web* :art:
	- DÉFI 2 : *Base de données* :bar_chart:
	- DÉFI 3 : *Programmation* :memo:
	- DÉFI 4 : *Système d'exploitation (GNU/Linux) & RegEX* :computer: :scroll:
	- DÉFI 5 : *Programmation & Réseau* :memo: :globe_with_meridians:

* **SAISON 4** *(2024)* :
	- DÉFI 1 : *Web & Base de données* :art: :bar_chart: 
	- DÉFI 2 : *Programmation & API* :memo: 
	- DÉFI 3 : *Système d'exploitation (GNU/Linux) & Réseau* :computer: :globe_with_meridians:
	- DÉFI 4 : *DevOps (Docker)* :whale:
	- DÉFI 5 : *Cybersécurité (CTF)* :bust_in_silhouette: :closed_lock_with_key:
